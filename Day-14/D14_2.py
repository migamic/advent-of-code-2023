import fileinput

def transpose_dish(d):
    return [''.join(l) for l in map(list, zip(*d))]


def tilt_part_left(part):
    num_rocks = part.count('O')
    num_empty = part.count('.')
    if num_rocks == 0 or num_empty == 0: return part
    return 'O'*num_rocks + '.'*num_empty

def tilt_part_right(part):
    num_rocks = part.count('O')
    num_empty = part.count('.')
    if num_rocks == 0 or num_empty == 0: return part
    return '.'*num_empty + 'O'*num_rocks

def tilt_row(row, left=True):
    parts = row.split('#')
    if left:
        return '#'.join([tilt_part_left(part) for part in parts])
    else:
        return '#'.join([tilt_part_right(part) for part in parts])


def tilt_cycle(dish):
    # North
    dish = transpose_dish([tilt_row(row,True) for row in transpose_dish(dish)])

    # West
    dish = [tilt_row(row,True) for row in dish]

    # South
    dish = transpose_dish([tilt_row(row, False) for row in transpose_dish(dish)])

    # East
    dish = [tilt_row(row, False) for row in dish]

    return dish


def compute_load(dish):
    load = 0
    for i,row in enumerate(dish):
        load += (len(dish)-i) * row.count('O')
    return load


def make_hashable(dish):
    return tuple(dish)


def print_dish(dish):
    for row in dish:
        print(row)
    print()

if __name__ == "__main__":

    dish = []
    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        dish.append(line)

    CACHE = {}
    HISTORY = []
    N = 1000000000
    for i in range(N):
        if make_hashable(dish) in CACHE:
            cycle_length = i - CACHE[make_hashable(dish)]
            offset = CACHE[make_hashable(dish)]
            dish = HISTORY[(N-offset)%cycle_length + offset]
            break

        CACHE[make_hashable(dish)] = i
        HISTORY.append(dish)

        dish = tilt_cycle(dish)

    result = compute_load(dish)
        
    print('Result:', result)
