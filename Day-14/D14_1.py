import fileinput

def transpose_dish(d):
    return [''.join(l) for l in map(list, zip(*d))]


def tilt_part_left(part):
    num_rocks = part.count('O')
    num_empty = part.count('.')
    if num_rocks == 0 or num_empty == 0: return part
    return 'O'*num_rocks + '.'*num_empty


def tilt_row_left(row):
    parts = row.split('#')
    return '#'.join([tilt_part_left(part) for part in parts])


def compute_load(dish):
    load = 0
    for i,row in enumerate(dish):
        load += (len(dish)-i) * row.count('O')
    return load


def print_dish(dish):
    for row in dish:
        print(row)
    print()

if __name__ == "__main__":

    dish = []
    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        dish.append(line)

    tilted_dish = transpose_dish([tilt_row_left(row) for row in transpose_dish(dish)])

    result = compute_load(tilted_dish)
        
    print('Result:', result)
