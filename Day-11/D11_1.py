import fileinput
import re
from itertools import combinations


def find_galaxies(image):
    galaxies = []

    for i,line in enumerate(image):
        if '#' in line:
            [galaxies.append((i, g.start())) for g in re.finditer('#', line)]

    return galaxies



def find_empty_rows(image, galaxies):
    all_rows = set(range(len(image)))
    full_rows = set([g[0] for g in galaxies])
    return all_rows-full_rows

def find_empty_cols(image, galaxies):
    all_cols = set(range(len(image[0])))
    full_cols = set([g[1] for g in galaxies])
    return all_cols-full_cols


def distance(g1, g2, empty_rows, empty_cols):
    dist = abs(g1[0]-g2[0]) + abs(g1[1]-g2[1]) # Base L1 distance

    # Add universe expansion
    # For every empty row and column between the two galaxies, add 1 to the distance
    for r in empty_rows:
        if (g1[0] < r < g2[0]) or (g2[0] < r < g1[0]): dist += 1
    for c in empty_cols:
        if (g1[1] < c < g2[1]) or (g2[1] < c < g1[1]): dist += 1

    return dist


if __name__ == "__main__":

    image = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        image.append(line)

    galaxies = find_galaxies(image)
    empty_rows = find_empty_rows(image, galaxies)
    empty_cols = find_empty_cols(image, galaxies)

    result = 0
    for (g1,g2) in combinations(galaxies,2):
        result += distance(g1, g2, empty_rows, empty_cols)
        
    print('Result:', result)
