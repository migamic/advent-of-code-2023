import fileinput
from functools import cmp_to_key
from collections import Counter


def get_num_cards(cards):
    num_cards = []
    for c in cards:
        if c.isdigit():
            num_cards.append(int(c))
        elif c == 'T': num_cards.append(10)
        elif c == 'J': num_cards.append(1) # Jokers have the smallest value now
        elif c == 'Q': num_cards.append(12)
        elif c == 'K': num_cards.append(13)
        elif c == 'A': num_cards.append(14)
        else: assert(False)
    return num_cards

def get_hand_type(cards):
    # 0: high card; all different
    # 1: one pair; two equal, others different
    # 2: two pair; two different pairs, others different
    # 3: three of a kind; three equal, others different
    # 4: full house; three equal, other two equal
    # 5: four of a kind; four equal, other different
    # 6: five of a kind; five equal

    num_J = cards.count(1)
    cards = list(filter((1).__ne__, cards)) # Remove Js

    counts = [v for k,v in Counter(cards).items()]

    if 5 in counts: return 6

    if 4 in counts:
        if num_J > 0: return 6
        return 5

    if 3 in counts and 2 in counts: return 4

    if 3 in counts:
        if num_J == 1: return 5
        if num_J == 2: return 6
        return 3

    if 2 in counts and counts.count(2) == 2:
        if num_J == 1: return 4
        return 2

    if 2 in counts:
        if num_J == 1: return 3
        if num_J == 2: return 5
        if num_J == 3: return 6
        return 1

    if num_J == 1: return 1
    if num_J == 2: return 3
    if num_J == 3: return 5
    if num_J > 3: return 6
    return 0


def parse_hand(line):
    cards = line.split(' ')[0]
    bid   = line.split(' ')[1]

    num_cards = get_num_cards(cards)
    hand_type = get_hand_type(num_cards)
    return {'cards': num_cards, 'type': hand_type, 'bid': int(bid)}


def compare(h1, h2):
    if h1['type'] != h2['type']:
        return h1['type'] - h2['type']

    for (c1,c2) in zip(h1['cards'], h2['cards']):
        if c1 != c2:
            return c1 - c2

    return 0


def get_result(hands):
    result = 0
    for i,h in enumerate(hands):
        result += (i+1)*h['bid']

    return result


if __name__ == "__main__":

    hands = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        hands.append(parse_hand(line))

    # Sort from worst (lowest rank) to best (highest rank)
    hands = sorted(hands, key=cmp_to_key(compare))

    result = get_result(hands)
        
    print('Result:', result)
