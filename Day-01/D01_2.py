import fileinput
import re

values = []

DIGIT_MAP = {
        0 : r"zero",
        1 : r"one",
        2 : r"two",
        3 : r"three",
        4 : r"four",
        5 : r"five",
        6 : r"six",
        7 : r"seven",
        8 : r"eight",
        9 : r"nine"
        }


def find_calibration(line):
    # Find first element
    best_pos = len(line)
    val_1 = -1
    for i in range(10):
        idx = line.find(str(i))
        if idx >= 0 and idx < best_pos:
            best_pos = idx
            val_1 = i
        idx = line.find(DIGIT_MAP[i])
        if idx >= 0 and idx < best_pos:
            best_pos = idx
            val_1 = i
        
    # Find last element
    best_pos = len(line)
    val_2 = -1
    for i in range(10):
        idx = line[::-1].find(str(i))
        if idx >= 0 and idx < best_pos:
            best_pos = idx
            val_2 = i
        idx = line[::-1].find(DIGIT_MAP[i][::-1])
        if idx >= 0 and idx < best_pos:
            best_pos = idx
            val_2 = i

    return ''.join([str(val_1), str(val_2)])


for line in fileinput.input():
    line = line.rstrip() # Remove newline char
    cal_val = find_calibration(line)
    values.append(int(cal_val))

print('Result:', sum(values))
