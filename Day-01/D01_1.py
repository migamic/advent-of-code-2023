import fileinput

values = []

for line in fileinput.input():
    line = line.rstrip() # Remove newline char
    digits = ''.join(filter(lambda x: x.isdigit(), line))
    cal_val = int(''.join([digits[0], digits[-1]]))
    values.append(cal_val)

print('Result:', sum(values))
