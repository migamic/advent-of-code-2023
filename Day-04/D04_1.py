import fileinput
import re


def get_points(line):
    numbers = line.split(': ')[1]
    (win,mine) = numbers.split('|')
    win_set = set(map(int, re.findall(r'\d+', win)))
    mine_set = set(map(int, re.findall(r'\d+', mine)))

    matches = set.intersection(win_set, mine_set)

    if len(matches) == 0: return 0
    return 2**(len(matches)-1)


if __name__ == "__main__":

    scores = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        scores.append(get_points(line))
        
    print('Result:', sum(scores))
