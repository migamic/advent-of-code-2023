import fileinput
import re


def get_matches(card):
    numbers = card.split(': ')[1]
    (win,mine) = numbers.split('|')
    win_set = set(map(int, re.findall(r'\d+', win)))
    mine_set = set(map(int, re.findall(r'\d+', mine)))
    matches = set.intersection(win_set, mine_set)
    return len(matches)


if __name__ == "__main__":

    card_matches = [None]

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        card_matches.append(get_matches(line))
        

    card_amount = len(card_matches)-1 # -1 since there is no card 0
    cards_added = [-1]*len(card_matches) # How many new cards every card adds into the deck

    # Iterate backwards, reusing results
    for i in range(len(card_matches)-1,0,-1):
        assert cards_added[i] == -1
        matches = card_matches[i]
        max_card = min(i+matches, len(card_matches))
        new_cards = 0

        for j in range(i+1, max_card+1):
            assert cards_added[j] >= 0
            new_cards += cards_added[j]

        card_amount += new_cards
        cards_added[i] = new_cards + 1

    print('Result:', card_amount)
