import fileinput
import re


def get_seeds(seeds_line):
    nums = [int(s) for s in re.findall(r'\d+', seeds_line)]
    it = iter(nums)
    return zip(it, it)


def build_map(lines):
    curr_map = []
    for line in lines[1:]:
        curr_map.append([int(s) for s in re.findall(r'\d+', line)])

    return curr_map


def get_mappings(mapping_lines):
    mappings = []
    curr_lines = []
    for line in mapping_lines:
        if len(line) > 0:
            curr_lines.append(line)
        else:
            mappings.append(build_map(curr_lines))
            curr_lines = []


    mappings.append(build_map(curr_lines))
    return mappings


def sort_ranges(ranges):
    # Sort by initial value, increasingly
    return sorted(ranges, key=lambda x: x[0])

def sort_mapping(mapping):
    # Sort by ini_source, increasingly
    return sorted(mapping, key=lambda x: x[1])


# Splits the original ranges, so if there is a range [A,B] and a value of ini_source S so that A < S <= B,
# two new ranges [A,S-1] and [S,B] are formed
def split_ranges(ranges, ini_sources):
    new_ranges = []

    for (a,b) in ranges:
        breakpoints = []
        for s in ini_sources:
            if a < s and s <= b:
                breakpoints.append(s)

        if len(breakpoints) == 0:
            new_ranges.append((a,b))
        else:
            # Note that ini_sources is sorted, so breakpoints is too
            new_ranges.append((a, breakpoints[0]-1))
            for i in range(0,len(breakpoints)-1):
                new_ranges.append((breakpoints[i], breakpoints[i+1]-1))
            new_ranges.append((breakpoints[-1], b))

    return new_ranges

def map_value(mapping, val):

    for case in mapping:
        ini_dest = case[0]
        ini_source = case[1]
        range_len = case[2]

        if val-ini_source >= 0 and val-ini_source < range_len:
            return val-ini_source + ini_dest

    return val


def apply_mapping(mapping, ranges):
    ranges = sort_ranges(ranges)
    mapping = sort_mapping(mapping)

    ini_dests, ini_sources, range_lens = zip(*mapping)

    new_ranges = split_ranges(ranges, ini_sources)

    # Every sp_range is now guaranteed to map to the same region
    # Then, simply map the beginning and end

    return [(map_value(mapping,ini), map_value(mapping,end)) for (ini, end) in new_ranges]


def get_min_value(ranges):
    starts, ends = zip(*ranges)
    return min(starts)


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    seeds = get_seeds(lines[0])
    mappings = get_mappings(lines[2:])

    ranges = [] # Each range is a tuple (start, end). Both included
    for (init, amount) in seeds:
        ranges.append((init, init+amount))
    
    for m in mappings:
        ranges = apply_mapping(m, ranges)

    result = get_min_value(ranges)

    print('Result:', result)
