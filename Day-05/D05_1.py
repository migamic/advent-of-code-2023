import fileinput
import re


def get_seeds(seeds_line):
    return [int(s) for s in re.findall(r'\d+', seeds_line)]


def build_map(lines):
    curr_map = []
    for line in lines[1:]:
        curr_map.append([int(s) for s in re.findall(r'\d+', line)])

    return curr_map


def get_mappings(mapping_lines):
    mappings = []
    curr_lines = []
    for line in mapping_lines:
        if len(line) > 0:
            curr_lines.append(line)
        else:
            mappings.append(build_map(curr_lines))
            curr_lines = []


    mappings.append(build_map(curr_lines))
    return mappings


def apply_mapping(mapping, val):
    for case in mapping:
        ini_dest = case[0]
        ini_source = case[1]
        range_len = case[2]

        if val-ini_source > 0 and val-ini_source < range_len:
            return val-ini_source + ini_dest

    return val


def find_location(mappings, seed):
    val = seed

    for m in mappings:
        val = apply_mapping(m, val)

    return val


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    seeds = get_seeds(lines[0])
    mappings = get_mappings(lines[2:])

    locations = []

    for s in seeds:
        locations.append(find_location(mappings, s))
        
    print('Result:', min(locations))
