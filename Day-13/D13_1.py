import fileinput


def transpose_pattern(p):
    return [''.join(l) for l in map(list, zip(*p))]


def check_mirror(p,r1,r2):
    assert r1 < r2

    while r1 < r2:
        if p[r1] != p[r2]: return False
        r1 += 1
        r2 -= 1

    return True


def find_horizontal_top(p):
    for i in range(1,len(p),2):
        if p[0] == p[i]:
            if check_mirror(p,0,i):
                #print(f'Match line 0 ({p[0]}) with line {i} ({p[i]})')
                return (i+1)//2
    return -1

def find_horizontal_line(p):
    # Find a horizontal line that splits the pattern into top and bottom
    # Observation: either the top row or the bottom row will be repeated somewhere
    top_rep = find_horizontal_top(p)
    if top_rep != -1: return top_rep

    bot_rep = find_horizontal_top(p[::-1])
    if bot_rep != -1: return len(p)-bot_rep

    return -1


def find_vertical_line(p):
    return find_horizontal_line(transpose_pattern(p))


if __name__ == "__main__":

    patterns = [[]]

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        if len(line) == 0:
            patterns.append([])
        else:
            patterns[-1].append(line)


    result = 0

    for p in patterns:
        hl = find_horizontal_line(p)
        if hl == -1:
            vl = find_vertical_line(p)
            assert vl != -1, 'No mirror was found'
            result += vl
        else:
            result += 100*hl
        
    print('Result:', result)
