import fileinput


def transpose_pattern(p):
    return [''.join(l) for l in map(list, zip(*p))]


def check_mirror(p,r1,r2):
    assert r1 < r2

    while r1 < r2:
        if p[r1] != p[r2]: return False
        r1 += 1
        r2 -= 1

    return True


def find_horizontal_top(p):
    for i in range(1,len(p),2):
        if p[0] == p[i]:
            if check_mirror(p,0,i):
                #print(f'Match line 0 ({p[0]}) with line {i} ({p[i]})')
                return (i+1)//2
    return -1

def find_horizontal_line(p):
    # Find a horizontal line that splits the pattern into top and bottom
    # Observation: either the top row or the bottom row will be repeated somewhere
    top_rep = find_horizontal_top(p)
    bot_rep = find_horizontal_top(p[::-1])
    if bot_rep != -1: bot_rep = len(p)-bot_rep
    return(top_rep, bot_rep)


def find_vertical_line(p):
    return find_horizontal_line(transpose_pattern(p))


def find_number(p, orig_num):
    (hl_t,hl_b) = find_horizontal_line(p)
    (vl_t,vl_b) = find_vertical_line(p)

    if hl_t != -1 and 100*hl_t != orig_num:
        return 100*hl_t
    if hl_b != -1 and 100*hl_b != orig_num:
        return 100*hl_b
    if vl_t != -1 and vl_t != orig_num:
        return vl_t
    if vl_b != -1 and vl_b != orig_num:
        return vl_b
    return -1


def swap_smudge(p, i, j):
    new_char = '#' if p[i][j] == '.' else '.'
    row = list(p[i])
    row[j] = new_char
    p[i] = ''.join(row)
    return p


def alternative_patterns(p):
    altp = []
    for i in range(len(p)):
        for j in range(len(p[i])):
            altp.append(swap_smudge(p.copy(),i,j))
    return altp


def print_pattern(p):
    for row in p:
        print(row)
    print()


if __name__ == "__main__":

    patterns = [[]]

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        if len(line) == 0:
            patterns.append([])
        else:
            patterns[-1].append(line)


    result = 0

    for p in patterns:
        orig_num = find_number(p,-2)
        assert orig_num != -1
        found = False
        for a in alternative_patterns(p):
            if not found:
                new_num = find_number(a, orig_num)
                if new_num != -1:
                    found = True
                    result += new_num
        assert found, 'No smudge found'
        
    print('Result:', result)
