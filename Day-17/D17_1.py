import fileinput
from queue import PriorityQueue


def get_adjacent(pos):
    return [(pos[0]-1,pos[1]), (pos[0]+1,pos[1]), (pos[0],pos[1]-1), (pos[0],pos[1]+1)]

def get_next_pos(pos,prev_pos):
    diff = (pos[0]-prev_pos[0], pos[1]-prev_pos[1])
    return (pos[0]+diff[0], pos[1]+diff[1])


def find_min_heat(city, ini_pos, fin_pos):

    state = {'pos' : ini_pos, 'prev_pos' : (-1,-1) , 'straight' : 0}

    counter = 0 # Used to avoid dict comparison
    visited = set()

    q = PriorityQueue()
    q.put((0,counter,state))

    while not q.empty():
        prio, _, item = q.get()
        hash_item = frozenset(item.items())
        if hash_item in visited:
            continue
        visited.add(hash_item)

        if item['pos'] == fin_pos:
            return prio

        np = get_next_pos(item['pos'],item['prev_pos'])
        for a in get_adjacent(item['pos']):
            counter += 1
            if a in city and a != item['prev_pos']:
                if a == np:
                    if item['straight'] < 2:
                        new_state = {'pos' : a, 'prev_pos' : item['pos'] , 'straight' : item['straight']+1}
                    else:
                        continue
                else:
                    new_state = {'pos' : a, 'prev_pos' : item['pos'] , 'straight' : 0}
                cost = prio + city[a]
                q.put((cost,counter,new_state))




    assert False, 'All paths explored and destination not reached'
    



if __name__ == "__main__":

    city = {}
    ini_pos = (0,0)
    fin_pos = (0,0)

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        for j,num in enumerate(line):
            city[(i,j)] = int(num)
            fin_pos = (i,j)

    min_heat = find_min_heat(city, ini_pos, fin_pos)
        
    print('Result:', min_heat)
