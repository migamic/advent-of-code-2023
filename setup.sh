#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <number>"
  exit 1
fi

number="$1"
directory_name="Day-${number}"

if [ ! -d "$directory_name" ]; then
  mkdir "$directory_name"
  echo "Created directory: $directory_name"
else
  echo "Directory '$directory_name' already exists."
  exit
fi

cp template.py "$directory_name/D${number}_1.py"
cp template.py "$directory_name/D${number}_2.py"
touch "$directory_name/small.txt"
touch "$directory_name/input.txt"
