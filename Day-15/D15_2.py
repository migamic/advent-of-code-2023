import fileinput


def get_hash(step):
    val = 0

    for c in step:
        val += ord(c)
        val *= 17
        val %= 256

    return val


def get_step(s):
    step = {}

    if '-' in s:
        step['op'] = '-'
        step['id'] = s[:-1]
    else:
        step['op'] = '='
        step['id'] = s.split('=')[0]
        step['fl'] = int(s.split('=')[1])

    return step


def find_id(box, lid):
    ids = [b[0] for b in box]
    if lid not in ids: return -1
    return ids.index(lid)


def apply_step(boxes, s):
    bid = get_hash(s['id'])
    box = boxes[bid]
    
    if s['op'] == '-':
        lens_id = find_id(box, s['id'])
        if lens_id != -1:
            del(box[lens_id])
    else:
        lens_id = find_id(box, s['id'])
        if lens_id == -1:
            box.append((s['id'],s['fl']))
        else:
            box[lens_id] = (s['id'],s['fl'])

    boxes[bid] = box
    return boxes



def get_result(boxes):
    total_power = 0
    for i,box in enumerate(boxes):
        for j,lens in enumerate(box):
            total_power += (i+1) * (j+1) * lens[1]

    return total_power


if __name__ == "__main__":

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char

    steps = [get_step(s) for s in line.split(',')]
    
    boxes = [ [] for _ in range(256) ]

    for s in steps:
        boxes = apply_step(boxes, s)
        # print(s)
        # for i,box in enumerate(boxes):
        #     if len(box) > 0:
        #         print(i, box)
        # print('********************************************')


    print('Result:', get_result(boxes))
