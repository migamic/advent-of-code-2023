import fileinput


def get_hash(step):
    val = 0

    for c in step:
        val += ord(c)
        val *= 17
        val %= 256

    return val



if __name__ == "__main__":

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char

    steps = line.split(',')

    result = sum([get_hash(s) for s in steps])
        
    print('Result:', result)
