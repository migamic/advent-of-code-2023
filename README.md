# Advent of Code 2023

My submissions for Advent of Code 2023. Statements and data are available at [https://adventofcode.com/](https://adventofcode.com/).

I don't strive for the cleanest or most efficient implementations, sometimes I just want to program it quickly or try new stuff.

Each directory corresponds to a day of the event; each event has two problems. Data is expected to be read from standard input.
