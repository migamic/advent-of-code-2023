import fileinput
from math import lcm


def make_network(lines):
    network = {}
    for line in lines:
        name = line[:3]
        link1 = line[7:10]
        link2 = line[12:15]
        network[name] = (link1, link2)

    return network


def get_ini_pos(network):
    return [pos for pos in network.keys() if pos[-1] == 'A']


def apply_instructions(network, instructions):

    # Observation: in this particular input, all "threads" have a very well-defined cycle
    # in which they only reach a single Z node every N iterations

    cycle_lengths = []

    for pos in get_ini_pos(network):
        counter = 0

        while pos[-1] != 'Z':
            inst = instructions[counter%len(instructions)]
            if inst == 'L':
                pos = network[pos][0]
            else:
                pos = network[pos][1]
            counter += 1

        cycle_lengths.append(counter)

    return lcm(*cycle_lengths)


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    instructions = lines[0]

    network = make_network(lines[2:])

    result = apply_instructions(network, instructions)
        
    print('Result:', result)
