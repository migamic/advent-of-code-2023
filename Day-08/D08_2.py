import fileinput


def make_network(lines):
    network = {}
    for line in lines:
        name = line[:3]
        link1 = line[7:10]
        link2 = line[12:15]
        network[name] = (link1, link2)

    return network


def get_ini_pos(network):
    return [pos for pos in network.keys() if pos[-1] == 'A']


def apply_instructions(network, instructions):
    pos = get_ini_pos(network)
    print(pos)
    print(len(pos))
    counter = 0

    while not all(p[-1] == 'Z' for p in pos):
        inst = instructions[counter%len(instructions)]
        for p in range(len(pos)):
            if inst == 'L':
                pos[p] = network[pos[p]][0]
            else:
                pos[p] = network[pos[p]][1]
        counter += 1

    return counter


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    instructions = lines[0]

    network = make_network(lines[2:])

    result = apply_instructions(network, instructions)
        
    print('Result:', result)
