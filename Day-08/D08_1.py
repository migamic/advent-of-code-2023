import fileinput


def make_network(lines):
    network = {}
    for line in lines:
        name = line[:3]
        link1 = line[7:10]
        link2 = line[12:15]
        network[name] = (link1, link2)

    return network


def apply_instructions(network, instructions):
    pos = 'AAA'
    counter = 0

    while pos != 'ZZZ':
        inst = instructions[counter%len(instructions)]
        if inst == 'L':
            pos = network[pos][0]
        else:
            pos = network[pos][1]
        counter += 1

    return counter


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    instructions = lines[0]

    network = make_network(lines[2:])

    result = apply_instructions(network, instructions)
        
    print('Result:', result)
