import fileinput
import re
import itertools


# Store the result for every combination of remaining springs to decode and remaining groups number
CACHE = {}

def evaluate(springs, numbers):
    groups = [len(g) for g in re.findall(r'#+', springs)]
    return groups == numbers


def check_still_possible(springs, numbers):
    if springs.count('#') > sum(numbers): return False # Too many '#'
    if springs.count('#') + springs.count('?') < sum(numbers): return False # Too few '#'

    hash_groups = [len(g) for g in re.findall(r'#+', springs)]
    if len(hash_groups) > 0:
        if max(hash_groups) > max(numbers): return False
        if springs[0] == '#' and hash_groups[0] > numbers[0]: return False

    return True


def find_cache_key(springs, numbers):
    return (springs, tuple(numbers))

def recursive_possibilities(springs, numbers):
    first_missing = springs.find('?')
    if first_missing == -1:
        return evaluate(springs, numbers) # Base case, all filled up

    # Remove all completed groups from the numbers list
    groups = [len(g)-1 for g in re.findall(r'#+\.', springs[:first_missing])]
    if groups != numbers[:len(groups)]: return False
    numbers = numbers[len(groups):]

    # Remove completed groups and leading '.' from the springs string
    springs = re.sub(r'^(\.*#*)+\.', '', springs)

    # Check if in cache
    cache_key = find_cache_key(springs, numbers)
    if cache_key in CACHE: return CACHE[cache_key]

    # Prunning. Don't continue if it is already unfeasible
    if not check_still_possible(springs, numbers): return 0

    # Else try subsituting the first missing with '.' or '#' and recursively continue
    count1 = recursive_possibilities(springs.replace('?', '.', 1), numbers)
    count2 = recursive_possibilities(springs.replace('?', '#', 1), numbers)

    # Store in cache
    CACHE[cache_key] = count1 + count2

    return count1 + count2


def count_possibilities(line):
    springs,numbers = line.split(' ')
    numbers = [int(n) for n in numbers.split(',')]

    # Expand input
    springs = (springs+'?') * 5
    springs = springs[:-1] # Remove accidental last '?'
    numbers = numbers * 5

    return recursive_possibilities(springs, numbers)


if __name__ == "__main__":

    result = 0

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        possib =  count_possibilities(line)
        print(i, possib)
        result += possib
        
    print('Result:', result)
