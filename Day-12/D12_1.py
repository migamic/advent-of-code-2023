import fileinput
import re
import itertools

def substitute_missing(springs, p):
    new_springs = []

    miss_idx = 0
    for s in springs:
        if s == '?':
            new_springs.append(p[miss_idx])
            miss_idx += 1
        else:
            new_springs.append(s)

    return ''.join(new_springs)


def evaluate(springs, numbers):
    groups = [len(g) for g in re.findall(r'#+', springs)]
    return groups == numbers


def count_possibilities(line):

    count = 0

    springs,numbers = line.split(' ')
    numbers = [int(n) for n in numbers.split(',')]

    num_missing = springs.count('?')

    # Generate all 2^N possibilities. Test if it is possible
    possibilities = list(itertools.product(['.','#'], repeat=num_missing))

    for p in possibilities:
        new_springs = substitute_missing(springs, p)
        if evaluate(new_springs, numbers): count += 1


    return count


if __name__ == "__main__":

    result = 0

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        result += count_possibilities(line)
        
    print('Result:', result)
