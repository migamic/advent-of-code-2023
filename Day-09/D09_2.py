import fileinput
import re


def predict_next_value(line):
    if all(i==0 for i in line): return 0 # Base case

    # Compute differences
    diff_line = [line[i+1]-line[i] for i in range(len(line)-1)]

    # Recursively predict next value
    pred_value = predict_next_value(diff_line)
    return line[0] - pred_value



if __name__ == "__main__":
    values = []
    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        numbers = [int(s) for s in line.split(' ')]
        values.append(predict_next_value(numbers))
        
    print('Result:', sum(values))
