import fileinput


def add_boundary(pipes):
    pipes = ['.' + p + '.' for p in pipes]
    pipes.insert(0, '.'*len(pipes[0]))
    pipes.append('.'*len(pipes[0]))

    return pipes


def find_S(pipes):
    for i,p in enumerate(pipes):
        if 'S' in p:
            return (i,p.find('S'))
    assert False


def get_pipe(pipes, pos):
    return pipes[pos[0]][pos[1]]


# Returns True iff p1 and p2 are connected. They are assumed to be adjacent
def can_go(pipes, p1, p2):
    if p1[1] == p2[1]:
        # Vertical adjacency
        if p1[0] > p2[0]: p1,p2 = p2,p1 # Assume that p1 is on top

        c1 = get_pipe(pipes,p1)
        c2 = get_pipe(pipes,p2)

        if c1 not in {'|', 'F', '7', 'S'}: return False
        if c2 not in {'|', 'L', 'J', 'S'}: return False
        return True

    if p1[0] == p2[0]:
        # Horizontal adjacency
        if p1[1] > p2[1]: p1,p2 = p2,p1 # Assume that p1 is on the left

        c1 = get_pipe(pipes,p1)
        c2 = get_pipe(pipes,p2)

        if c1 not in {'-', 'F', 'L', 'S'}: return False
        if c2 not in {'-', 'J', '7', 'S'}: return False
        return True

    assert False, f'{p1} and {p2} are not adjacent'


def find_max_length(pipes, S):
    counter = 0

    visited = []
    curr_pos = S

    while counter == 0 or curr_pos != S:
        # Try going in any of the four directions. First one available, go
        # If all directions (there will only be 2 possible) are visited, the loop is finished

        # Up, down, left, right
        cands = [(curr_pos[0]-1, curr_pos[1]), (curr_pos[0]+1, curr_pos[1]), (curr_pos[0], curr_pos[1]-1), (curr_pos[0], curr_pos[1]+1)]

        next_found = False
        for cand in cands:
            if not next_found and not cand in visited and can_go(pipes, curr_pos, cand):
                next_found = True
                visited.append(curr_pos)
                curr_pos = cand

        counter += 1
        if not next_found:
            return counter/2

    assert False



if __name__ == "__main__":

    pipes = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        pipes.append(line)
    
    # Add a boundary of '.' to avoid dealing with border conditions
    pipes = add_boundary(pipes)

    S = find_S(pipes)

    result = find_max_length(pipes, S)

    print('Result:', int(result))
