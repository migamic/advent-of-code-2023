import fileinput


def add_boundary(pipes):
    pipes = ['.' + p + '.' for p in pipes]
    pipes.insert(0, '.'*len(pipes[0]))
    pipes.append('.'*len(pipes[0]))

    return pipes


def find_S(pipes):
    for i,p in enumerate(pipes):
        if 'S' in p:
            return (i,p.find('S'))
    assert False


def get_pipe(pipes, pos):
    return pipes[pos[0]][pos[1]]


# Returns True iff p1 and p2 are connected. They are assumed to be adjacent
def can_go(pipes, p1, p2):
    if p1[1] == p2[1]:
        # Vertical adjacency
        if p1[0] > p2[0]: p1,p2 = p2,p1 # Assume that p1 is on top

        c1 = get_pipe(pipes,p1)
        c2 = get_pipe(pipes,p2)

        if c1 not in {'|', 'F', '7', 'S'}: return False
        if c2 not in {'|', 'L', 'J', 'S'}: return False
        return True

    if p1[0] == p2[0]:
        # Horizontal adjacency
        if p1[1] > p2[1]: p1,p2 = p2,p1 # Assume that p1 is on the left

        c1 = get_pipe(pipes,p1)
        c2 = get_pipe(pipes,p2)

        if c1 not in {'-', 'F', 'L', 'S'}: return False
        if c2 not in {'-', 'J', '7', 'S'}: return False
        return True

    assert False, f'{p1} and {p2} are not adjacent'


def find_loop(pipes, S):
    counter = 0

    visited = []
    curr_pos = S

    while counter == 0 or curr_pos != S:
        # Try going in any of the four directions. First one available, go
        # If all directions (there will only be 2 possible) are visited, the loop is finished

        # Up, down, left, right
        cands = [(curr_pos[0]-1, curr_pos[1]), (curr_pos[0]+1, curr_pos[1]), (curr_pos[0], curr_pos[1]-1), (curr_pos[0], curr_pos[1]+1)]

        next_found = False
        for cand in cands:
            if not next_found and not cand in visited and can_go(pipes, curr_pos, cand):
                next_found = True
                visited.append(curr_pos)
                curr_pos = cand

        counter += 1
        if not next_found:
            visited.append(curr_pos)
            return set(visited)

    assert False



def fill_S(pipes, loop, S):
    up = (S[0]-1, S[1])
    down = (S[0]+1, S[1])
    left = (S[0], S[1]-1)
    right = (S[0], S[1]+1)

    if can_go(pipes, S, up) and can_go(pipes, S, down): s_char = '|'
    elif can_go(pipes, S, up) and can_go(pipes, S, left): s_char = 'J'
    elif can_go(pipes, S, up) and can_go(pipes, S, right): s_char = 'L'
    elif can_go(pipes, S, left) and can_go(pipes, S, right): s_char = '-'
    elif can_go(pipes, S, down) and can_go(pipes, S, left): s_char = '7'
    elif can_go(pipes, S, down) and can_go(pipes, S, right): s_char = 'F'

    pipes[S[0]] = pipes[S[0]].replace('S',s_char)

    return pipes



def inside_loop(pipes, loop, pos):
    if pos in loop: return False

    # Trace a ray from pos to the outside, iff the number of times it crosses loop is odd, it is inside
    # Take into account the type of pipe to handle edge cases

    crossings = 0
    coming_from_left  = False
    coming_from_right = False
    for i in range(pos[0]): # From the top border, to pos
        if (i,pos[1]) in loop:
            c = get_pipe(pipes, (i,pos[1]))

            if c == '-': crossings += 1
            elif c == '7':
                assert not coming_from_left and not coming_from_right
                coming_from_left = True
            elif c == 'F':
                assert not coming_from_left and not coming_from_right
                coming_from_right = True
            elif c == 'J':
                assert coming_from_left or coming_from_right
                if coming_from_right: crossings += 1
                coming_from_left = False
                coming_from_right = False
            elif c == 'L':
                assert coming_from_left or coming_from_right
                if coming_from_left: crossings += 1
                coming_from_left = False
                coming_from_right = False
            # else (| or .) ignore

    return crossings % 2 == 1





def find_enclosed_area(pipes, loop):
    counter = 0

    for row in range(1,len(pipes)-1): # Ignore borders
        for col in range(1,len(pipes[0])-1):
            if inside_loop(pipes, loop, (row,col)):
                counter += 1

    return counter


if __name__ == "__main__":

    pipes = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        pipes.append(line)
    
    # Add a boundary of '.' to avoid dealing with border conditions
    pipes = add_boundary(pipes)

    S = find_S(pipes)

    loop = find_loop(pipes, S)

    pipes = fill_S(pipes, loop, S)

    result = find_enclosed_area(pipes, loop)

    print('Result:', result)
