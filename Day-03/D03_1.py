import fileinput


def is_symbol(schematic, row, col):
    if schematic[row][col] == '.': return False
    if schematic[row][col].isdigit(): return False
    return True


def symbols_around(schematic, row, col):
    # Symbol on top
    if row > 0 and is_symbol(schematic, row-1, col): return True

    # Symbol below
    if row < len(schematic)-1 and is_symbol(schematic, row+1, col): return True

    # Symbol on the left
    if col > 0 and is_symbol(schematic, row, col-1): return True

    # Symbol on the right 
    if col < len(schematic[row])-1 and is_symbol(schematic, row, col+1): return True

    # Symbol on top-left
    if row > 0 and col > 0 and is_symbol(schematic, row-1, col-1): return True

    # Symbol on top-right
    if row > 0 and col < len(schematic[row])-1 and is_symbol(schematic, row-1, col+1): return True

    # Symbol on bottom-left
    if row < len(schematic)-1 and col > 0 and is_symbol(schematic, row+1, col-1): return True

    # Symbol on bottom-right
    if row < len(schematic)-1 and col < len(schematic[row])-1 and is_symbol(schematic, row+1, col+1): return True

    return False


def is_PN(schematic, row, col, left_PN):

    # If the left neighbour is a PN, it is a PN
    if left_PN: return True

    # If there are any symbols around, it is a PN
    if symbols_around(schematic, row, col): return True

    # If it has a right neighbour, check if it is a PN. Then update accordingly
    if col < len(schematic[row])-1 and schematic[row][col+1].isdigit(): return is_PN(schematic, row, col+1, False)

    # Else, it is not a PN
    return False


# Returns a boolean array of the same shape as schematic
# True values indicate that there is a part number there
def get_part_numbers(schematic):

    mask = []
    for row in range(len(schematic)):
        mask_row = []
        for col in range(len(schematic[row])):
            if schematic[row][col].isdigit():
                if col == 0:
                    mask_row.append(is_PN(schematic, row, col, False))
                else:
                    mask_row.append(is_PN(schematic, row, col, mask_row[col-1]))
            else:
                mask_row.append(False)

        mask.append(mask_row)

    return mask


def get_result(schematic, part_num_mask):

    # Get numbers given the mask
    num_list = []
    for row in range(len(schematic)):
        curr_num = []
        for col in range(len(schematic[row])):
            if part_num_mask[row][col]: curr_num.append(schematic[row][col])
            else:
                if len(curr_num) > 0: num_list.append(''.join(curr_num))
                curr_num = []
        if len(curr_num) > 0: num_list.append(''.join(curr_num))

    return sum([int(n) for n in num_list])


if __name__ == "__main__":

    schematic = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        schematic.append(line)

    part_num_mask = get_part_numbers(schematic)

    result = get_result(schematic, part_num_mask)
        
    print('Result:', result)
