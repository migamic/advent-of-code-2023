import fileinput


def get_ID(schematic_row, col):

    # Go to the beginning of the number
    while col > 0 and schematic_row[col-1].isdigit():
        col -= 1

    # Go to the end of the number and store it
    num = []
    while col < len(schematic_row):
        if schematic_row[col].isdigit():
            num.append(schematic_row[col])
        else:
            assert len(num) > 0
            return int(''.join(num))
        col += 1

    return 0


# Return a new mask with every True element replaced by the part number
# Every False element is -1
def get_part_IDs(schematic, mask):
    IDs = []

    for row in range(len(mask)):
        row_IDs = []
        for col in range(len(mask[row])):
            if mask[row][col]:
                row_IDs.append(get_ID(schematic[row], col))
            else:
                row_IDs.append(-1)

        IDs.append(row_IDs)

    return IDs


def is_symbol(schematic, row, col):
    if schematic[row][col] == '.': return False
    if schematic[row][col].isdigit(): return False
    return True


def symbols_around(schematic, row, col):
    # Symbol on top
    if row > 0 and is_symbol(schematic, row-1, col): return True

    # Symbol below
    if row < len(schematic)-1 and is_symbol(schematic, row+1, col): return True

    # Symbol on the left
    if col > 0 and is_symbol(schematic, row, col-1): return True

    # Symbol on the right 
    if col < len(schematic[row])-1 and is_symbol(schematic, row, col+1): return True

    # Symbol on top-left
    if row > 0 and col > 0 and is_symbol(schematic, row-1, col-1): return True

    # Symbol on top-right
    if row > 0 and col < len(schematic[row])-1 and is_symbol(schematic, row-1, col+1): return True

    # Symbol on bottom-left
    if row < len(schematic)-1 and col > 0 and is_symbol(schematic, row+1, col-1): return True

    # Symbol on bottom-right
    if row < len(schematic)-1 and col < len(schematic[row])-1 and is_symbol(schematic, row+1, col+1): return True

    return False


def is_PN(schematic, row, col, left_PN):

    # If the left neighbour is a PN, it is a PN
    if left_PN: return True

    # If there are any symbols around, it is a PN
    if symbols_around(schematic, row, col): return True

    # If it has a right neighbour, check if it is a PN. Then update accordingly
    if col < len(schematic[row])-1 and schematic[row][col+1].isdigit(): return is_PN(schematic, row, col+1, False)

    # Else, it is not a PN
    return False


# Returns a boolean array of the same shape as schematic
# True values indicate that there is a part number there
def get_part_numbers(schematic):

    mask = []
    for row in range(len(schematic)):
        mask_row = []
        for col in range(len(schematic[row])):
            if schematic[row][col].isdigit():
                if col == 0:
                    mask_row.append(is_PN(schematic, row, col, False))
                else:
                    mask_row.append(is_PN(schematic, row, col, mask_row[col-1]))
            else:
                mask_row.append(False)

        mask.append(mask_row)

    return mask


def get_neighbours(IDs, row, col):
    neighbours = set()

    # Neighbour on top
    if row > 0 and IDs[row-1][col] >= 0: neighbours.add(IDs[row-1][col])

    # Neighbour below
    if row < len(IDs)-1 and IDs[row+1][col] >= 0: neighbours.add(IDs[row+1][col])

    # Neighbour on the left
    if col > 0 and IDs[row][col-1] >= 0: neighbours.add(IDs[row][col-1])

    # Neighbour on the right 
    if col < len(IDs[row])-1 and IDs[row][col+1] >= 0: neighbours.add(IDs[row][col+1])

    # Neighbour on top-left
    if row > 0 and col > 0 and IDs[row-1][col-1] >= 0: neighbours.add(IDs[row-1][col-1])

    # Neighbour on top-right
    if row > 0 and col < len(IDs[row]) and IDs[row-1][col+1] >= 0: neighbours.add(IDs[row-1][col+1])

    # Neighbour on bottom-left
    if row < len(IDs)-1 and col > 0 and IDs[row+1][col-1] >= 0: neighbours.add(IDs[row+1][col-1])

    # Neighbour on bottom-right
    if row < len(IDs)-1 and col < len(IDs[row])-1 and IDs[row+1][col+1] >= 0: neighbours.add(IDs[row+1][col+1])

    return neighbours



def get_result(schematic, part_num_IDs):
    ratios = []
    for row in range(len(schematic)):
        for col in range(len(schematic[row])):
            if schematic[row][col] == '*':
                neighbours = list(get_neighbours(part_num_IDs, row, col))
                if len(neighbours) == 2:
                    ratios.append(neighbours[0]*neighbours[1])


    return sum(ratios)


if __name__ == "__main__":

    schematic = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        schematic.append(line)

    part_num_mask = get_part_numbers(schematic)
    part_num_IDs = get_part_IDs(schematic, part_num_mask)

    result = get_result(schematic, part_num_IDs)
        
    print('Result:', result)
