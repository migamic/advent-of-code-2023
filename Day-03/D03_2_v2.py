import fileinput


def get_ID(schematic_row, col):

    # Go to the beginning of the number
    while col > 0 and schematic_row[col-1].isdigit():
        col -= 1

    # Go to the end of the number and store it
    num = []
    while col < len(schematic_row):
        if schematic_row[col].isdigit():
            num.append(schematic_row[col])
        else:
            assert len(num) > 0
            return int(''.join(num))
        col += 1

    return 0


def get_neighbours(schematic, row, col):
    neighbours = set()

    # Neighbour on top
    if row > 0 and schematic[row-1][col].isdigit(): neighbours.add(get_ID(schematic[row-1],col))

    # Neighbour below
    if row < len(schematic)-1 and schematic[row+1][col].isdigit(): neighbours.add(get_ID(schematic[row+1],col))

    # Neighbour on the left
    if col > 0 and schematic[row][col-1].isdigit(): neighbours.add(get_ID(schematic[row],col-1))

    # Neighbour on the right 
    if col < len(schematic[row])-1 and schematic[row][col+1].isdigit(): neighbours.add(get_ID(schematic[row],col+1))

    # Neighbour on top-left
    if row > 0 and col > 0 and schematic[row-1][col-1].isdigit(): neighbours.add(get_ID(schematic[row-1],col-1))

    # Neighbour on top-right
    if row > 0 and col < len(schematic[row]) and schematic[row-1][col+1].isdigit(): neighbours.add(get_ID(schematic[row-1],col+1))

    # Neighbour on bottom-left
    if row < len(schematic)-1 and col > 0 and schematic[row+1][col-1].isdigit(): neighbours.add(get_ID(schematic[row+1],col-1))

    # Neighbour on bottom-right
    if row < len(schematic)-1 and col < len(schematic[row])-1 and schematic[row+1][col+1].isdigit(): neighbours.add(get_ID(schematic[row+1],col+1))

    return neighbours



def get_result(schematic):
    ratios = []
    for row in range(len(schematic)):
        for col in range(len(schematic[row])):
            if schematic[row][col] == '*':
                neighbours = list(get_neighbours(schematic, row, col))
                if len(neighbours) == 2:
                    ratios.append(neighbours[0]*neighbours[1])

    return sum(ratios)


if __name__ == "__main__":

    schematic = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        schematic.append(line)

    result = get_result(schematic)
        
    print('Result:', result)
