import fileinput
import re


def get_ID(schematic_row, col):

    # Go to the beginning of the number
    while col > 0 and schematic_row[col-1].isdigit():
        col -= 1

    # Go to the end of the number and store it
    num = []
    while col < len(schematic_row):
        if schematic_row[col].isdigit():
            num.append(schematic_row[col])
        else:
            assert len(num) > 0
            return int(''.join(num))
        col += 1

    if col == len(schematic_row): return int(''.join(num))
    return 0


def get_neighbours(schematic, row, col):
    neighbours = []

    # Neighbour on the left
    if col > 0 and schematic[row][col-1].isdigit(): neighbours.append(get_ID(schematic[row],col-1))

    # Neighbour on the right 
    if col < len(schematic[row])-1 and schematic[row][col+1].isdigit(): neighbours.append(get_ID(schematic[row],col+1))

    # Upper row
    if row > 0:
        # Trim the string to only adjacent numbers
        min_idx = 0 if col == 0 else col-1
        while min_idx > 0 and schematic[row-1][min_idx].isdigit(): min_idx -= 1

        max_idx = len(schematic[row])-1 if col == len(schematic[row])-1 else col+1
        while max_idx < len(schematic[row]) and schematic[row-1][max_idx].isdigit(): max_idx += 1


        neighbours += map(int, re.findall(r'\d+', schematic[row-1][min_idx:max_idx+1]))

    # Lower row
    if row < len(schematic)-1:
        # Trim the string to only adjacent numbers
        min_idx = 0 if col == 0 else col-1
        while min_idx > 0 and schematic[row+1][min_idx].isdigit(): min_idx -= 1

        max_idx = len(schematic[row])-1 if col == len(schematic[row])-1 else col+1
        while max_idx < len(schematic[row]) and schematic[row+1][max_idx].isdigit(): max_idx += 1

        neighbours += map(int, re.findall(r'\d+', schematic[row+1][min_idx:max_idx+1]))

    return neighbours



def get_result(schematic):
    ratios = []
    for row in range(len(schematic)):
        for col in range(len(schematic[row])):
            if schematic[row][col] == '*':
                neighbours = get_neighbours(schematic, row, col)
                if len(neighbours) == 2:
                    ratios.append(neighbours[0]*neighbours[1])

    return sum(ratios)


if __name__ == "__main__":

    schematic = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        schematic.append(line)

    result = get_result(schematic)
        
    print('Result:', result)
