import fileinput

# Returns true iff the game would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes
def valid_game(game):
    for s in game['sets']:
        if s[0] > 12: return False
        if s[1] > 13: return False
        if s[2] > 14: return False
    return True

def parse_game_info(line):

    game = {}

    # Get the ID
    game['id'] = int(line.split(':')[0].split(' ')[1])

    # Get the different sets
    # A game is a list of sets. Each set is a triplet [r,g,b]
    sets = []
    for s in line.split(':')[1].split(';'):
        balls = [0,0,0]
        for c in s[1:].split(', '):
            (amount, color) = c.split(' ')
            if color == 'red': balls[0] = int(amount)
            elif color == 'green': balls[1] = int(amount)
            elif color == 'blue': balls[2] = int(amount)
        sets.append(balls)
    game['sets'] = sets

    return game


if __name__ == "__main__":
    ids = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        game = parse_game_info(line)
        
        if valid_game(game):
            ids.append(game['id'])

    print('Result:', sum(ids))
