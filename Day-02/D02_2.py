import fileinput


def compute_game_power(game):
    rs = [s[0] for s in game['sets']]
    gs = [s[1] for s in game['sets']]
    bs = [s[2] for s in game['sets']]

    return max(rs)*max(gs)*max(bs)


def parse_game_info(line):

    game = {}

    # Get the ID
    game['id'] = int(line.split(':')[0].split(' ')[1])

    # Get the different sets
    # A game is a list of sets. Each set is a triplet [r,g,b]
    sets = []
    for s in line.split(':')[1].split(';'):
        balls = [0,0,0]
        for c in s[1:].split(', '):
            (amount, color) = c.split(' ')
            if color == 'red': balls[0] = int(amount)
            elif color == 'green': balls[1] = int(amount)
            elif color == 'blue': balls[2] = int(amount)
        sets.append(balls)
    game['sets'] = sets

    return game


if __name__ == "__main__":
    result = 0

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        game = parse_game_info(line)
        result += compute_game_power(game)

    print('Result:', result)
