import fileinput
import re


def num_ways_to_win(race):
    total_time = race[0]
    distance = race[1]
    num = 0
    for i in range(total_time+1):
        if i*(total_time-i) > distance: num += 1

    return num


def get_races(lines):
    times = [int(s) for s in re.findall(r'\d+', lines[0])]
    dists = [int(s) for s in re.findall(r'\d+', lines[1])]

    return zip(times,dists)


if __name__ == "__main__":

    lines = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        lines.append(line)

    races = get_races(lines)

    result = 1
    for race in races:
        result *= num_ways_to_win(race)
        
    print('Result:', result)
