import fileinput

def add_boundary(cave):
    cave = ['X' + l + 'X' for l in cave]
    cave.insert(0, 'X'*len(cave[0]))
    cave.append('X'*len(cave[0]))
    return cave


def get_pos(cave,beam):
    return cave[beam[0][0]][beam[0][1]]


def trace_beam_iter(cave, beam):

    pending = [beam]
    visited = set()

    while len(pending) > 0:
        beam = pending.pop(0)

        # Avoid infinite loops
        if beam in visited: continue

        curr_pos = get_pos(cave,beam)
        if curr_pos == 'X':
            continue
        elif curr_pos == '.':
            new_beam = ((beam[0][0]+beam[1][0],beam[0][1]+beam[1][1]),beam[1])
            pending.append(new_beam)
        elif curr_pos == '\\':
            new_dir = (beam[1][1],beam[1][0])
            new_beam = ((beam[0][0]+new_dir[0],beam[0][1]+new_dir[1]),new_dir)
            pending.append(new_beam)
        elif curr_pos == '/':
            new_dir = (-beam[1][1],-beam[1][0])
            new_beam = ((beam[0][0]+new_dir[0],beam[0][1]+new_dir[1]),new_dir)
            pending.append(new_beam)
        elif curr_pos == '-':
            if beam[1][0] == 0:
                new_beam = ((beam[0][0]+beam[1][0],beam[0][1]+beam[1][1]),beam[1])
                pending.append(new_beam)
            else:
                new_beam_1 = ((beam[0][0],beam[0][1]-1),(0,-1))
                new_beam_2 = ((beam[0][0],beam[0][1]+1),(0, 1))
                pending.append(new_beam_1)
                pending.append(new_beam_2)
        elif curr_pos == '|':
            if beam[1][1] == 0:
                new_beam = ((beam[0][0]+beam[1][0],beam[0][1]+beam[1][1]),beam[1])
                pending.append(new_beam)
            else:
                new_beam_1 = ((beam[0][0]-1,beam[0][1]),(-1,0))
                new_beam_2 = ((beam[0][0]+1,beam[0][1]),( 1,0))
                pending.append(new_beam_1)
                pending.append(new_beam_2)
        visited.add(beam)

    return len(set([b[0] for b in visited])) # Ignore directions


if __name__ == "__main__":

    cave = []

    # Read input
    for i,line in enumerate(fileinput.input()):
        line = str(line.rstrip()) # Remove newline char
        cave.append(line)

    # A beam is a tuple ((row,col),(dirV,dirH))
    beam = ((1,1),(0,1))

    result = trace_beam_iter(add_boundary(cave), beam)

    print('Result:', result)
